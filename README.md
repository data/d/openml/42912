# OpenML dataset: behavior-cervical

https://www.openml.org/d/42912

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: DR. Sobar, Prof. Rizanda Machmud, Adi Wijaya
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Cervical+Cancer+Behavior+Risk) - 2019
**Please cite**: [Paper](https://doi.org/10.1166/asl.2016.7980)  

**Cervical Cancer Behavior Risk Data Set**

The dataset contains 19 attributes regarding ca cervix behavior risk with class label is ca_cervix with 1 and 0 as values which means the respondent with and without ca cervix, respectively.

### Attribute information


This dataset consist of 18 attribute (comes from 8 variables, the name of variables is the first word in each attribute) 
1) behavior_eating 
2) behavior_personalHygine 
3) intention_aggregation 
4) intention_commitment 
5) attitude_consistency 
6) attitude_spontaneity 
7) norm_significantPerson 
8) norm_fulfillment 
9) perception_vulnerability 
10) perception_severity 
11) motivation_strength 
12) motivation_willingness 
13) socialSupport_emotionality 
14) socialSupport_appreciation 
15) socialSupport_instrumental 
16) empowerment_knowledge 
17) empowerment_abilities 
18) empowerment_desires 
19) ca_cervix (this is class attribute, 1=has cervical cancer, 0=no cervical cancer)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42912) of an [OpenML dataset](https://www.openml.org/d/42912). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42912/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42912/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42912/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

